package edu.ib.springio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Start {
    private ProductRepo productRepo;

    @Autowired
    public Start(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }
    @EventListener(ApplicationReadyEvent.class)
    public void runExample(){
        Product product= new Product("Mleko", 3.99f, true);
        productRepo.save(product);

        Product product1 = new Product("Chleb", 2.50f, false);
        productRepo.save(product1);

        Product product2 = new Product("Kakao", 7.35f, true);
        productRepo.save(product2);


        Iterable<Product> all = productRepo.findAll();
        System.out.println(all);





    }


}
